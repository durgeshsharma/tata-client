import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import classNames from "classnames";
import classes from "./Motor-insurance.module.scss";
import ToggleSwitch from "../../components/Common/ToggleSwitch";
import Modal from "../../components/Modal";
import NoClaimModal from "../../components/Modal/NoClaimModal";
import PremiumBreakupModal from "../../components/Modal/PremiumBreakupModal";
import Tabs from "../../components/Common/Tabs";
import Tab from "../../components/Common/Tabs/Tab";

const MotorQuote = () => {
  const [showClaimModal, setShowClaimModal] = useState(false);

  const claimModalClose = () => setShowClaimModal(false);
  const claimModalShow = () => setShowClaimModal(true);

  const [showPremiumModal, setShowPremiumModal] = useState(false);

  const premiumModalClose = () => setShowPremiumModal(false);
  const premiumModalShow = () => setShowPremiumModal(true);

  const [claimBonus, setClaimBonus] = useState(20);

  const changeClaimBonus = bonus => {
    setClaimBonus(bonus);
    claimModalClose();
  };

  const bonusList = [0, 5, 10, 15, 20];

  return (
    <>
      <div className="row">
        <div className="col-12">
          <p className="text-center" style={{ textTransform: "uppercase" }}>
            Your Insured Declared value
          </p>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
        <Tabs>
          <Tab iconClassName={'fa fa-headphones'}
              linkClassName={'custom-link'}>
          </Tab>
          <Tab iconClassName={'fa fa-headphones'}
              linkClassName={'custom-link'}>
          </Tab>
          <Tab iconClassName={'fa fa-headphones'}
              linkClassName={'custom-link'}>
          </Tab>
          <Tab iconClassName={'fa fa-headphones'}
              linkClassName={'custom-link'}>
          </Tab>
          <Tab iconClassName={'fa fa-headphones'}
              linkClassName={'custom-link'}>
          </Tab>
        </Tabs>
        </div>
      </div>
      <div className="row pt-2 pb-2">
        <div
          className="col-6"
          style={{ textTransform: "uppercase", fontSize: "12px" }}
        >
          Have You Claimed Your Previous Policy
        </div>
        <div className="col-6">
          <ToggleSwitch id="claimStatus" name="claimStatus" />
        </div>
      </div>
      <div className="row pt-2 pb-2">
        <div
          className="col-6"
          style={{ textTransform: "uppercase", fontSize: "12px" }}
        >
          No Claim Bonus%
        </div>
        <div
          className="col-3"
          style={{ marginLeft: "10px", borderBottom: "1px solid black" }}
        >
          {claimBonus}%{" "}
          <i
            className="fa fa-edit"
            onClick={() => {
              claimModalShow();
            }}
          ></i>
        </div>
      </div>
      <Modal
        toggle={() => {
          claimModalClose();
        }}
        modal={showClaimModal}
        children={NoClaimModal(bonusList)}
        title="No Claim Bonus"
      />
      <div className="row pt-2 pb-2">
        <div className="col-md-12 text-center">
          <div className={classNames(classes.scrolling_wrapper)}>
            <div className={classNames(classes.card)}>
              <p>Pack 1</p>
            </div>
            <div className={classNames(classes.card)}>
              <p>Pack 2</p>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <Formik
            initialValues={{ terms: false }}
            onSubmit={({ setSubmitting }) => {
              setSubmitting(false);
            }}
          >
            {({ touched, errors, isSubmitting }) => (
              <Form>
                <div className="form-group">
                  <label style={{ display: "flex" }} htmlFor="terms">
                    <div>
                      <Field
                        id="terms"
                        name="terms"
                        type="checkbox"
                        className={classNames("radio-button")}
                      />
                    </div>
                    <div style={{ paddingLeft: "10px", fontSize: "12px" }}>
                      I would like to be contact by TATA AIG Insurance company
                      limited. It will override my registry at the DNCR.
                    </div>
                  </label>
                </div>
              </Form>
            )}
          </Formik>
        </div>
        <div className="row"></div>
        <div className="row pt-1 pb-3 pl-2">
          <div className="col-12">
            <span
              style={{ textDecoration: "underline", fontStyle: "italic" }}
              onClick={() => {
                premiumModalShow();
              }}
            >
              View Premium Breakup
            </span>
          </div>
        </div>
        <Modal
          toggle={() => {
            premiumModalClose();
          }}
          modal={showPremiumModal}
          children={PremiumBreakupModal()}
          title="Premium Breakup"
        />
      </div>
      <button
        className={classNames(
          "btn",
          "btn-default",
          "btn-secondary",
          classes.sticky
        )}
      >
        Buy Now
      </button>
    </>
  );
};

export default MotorQuote;
