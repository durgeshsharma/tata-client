import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import classNames from "classnames";
import SubHeader from "../../../components/Landing/SubHeader";

const Car = () => {
  let initialForm = {
    mobileNo: "",
    terms: false
  };

  const mobileNoTest = /^[0][1-9]\d{9}$|^[1-9]\d{9}$/;

  let Validator = values => {
    let errors = {};
    if (values.mobileNo === "") {
      errors.mobileNo = "Mobile Number is required";
    } else if (!mobileNoTest.test(values.mobileNo)) {
      errors.mobileNo = "Invalid Format";
    }
    return errors;
  };

  return (
    <>
      <SubHeader />
      <div className="row">
        <div className="col-12" style={{ textAlign: "center" }}>
          <div
            className="w-100"
            style={{
              textTransform: "uppercase",
              textAlign: "left",
              fontSize: "12px"
            }}
          >
            Your Vehicle
          </div>
          <div>
            <img
              src="/motor.png"
              height="50"
              width="50"
              style={{ margin: "10px 0" }}
            />
          </div>
          <div style={{ fontSize: "12px", fontWeight: 600 }}>
            MH 04 CF 6756 |{" "}
            <span style={{ fontWeight: 500 }}>REGISTRATION DATE</span>
            :03/02/2015
          </div>
          <div style={{ fontSize: "12px", fontWeight: 600 }}>
            HYUNDAI | HONDA-CITY | IV-TEC{" "}
          </div>
        </div>
        <div className="col-12" style={{ margin: "20px auto" }}>
          <a
            style={{
              fontStyle: "italic",
              fontSize: "12px",
              textDecoration: "underline"
            }}
          >
            Not your vehicle?
          </a>
        </div>
        <div className="col-12">
          <Formik
            initialValues={initialForm}
            validate={Validator}
            onSubmit={({ setSubmitting }) => {
              setSubmitting(false);
            }}
          >
            {({ touched, errors, isSubmitting }) => (
              <Form>
                <div className="form-group">
                  <label style={{ fontSize: "14px" }} htmlFor="mobile">
                    Mobile Number
                  </label>
                  <Field
                    id="mobile"
                    type="text"
                    name="mobileNo"
                    placeholder="+91 987654321"
                    className={`form-control ${
                      touched.mobileNo && errors.mobileNo ? "is-invalid" : ""
                    }`}
                  />
                  <ErrorMessage
                    component="div"
                    name="mobileNo"
                    className="invalid-feedback"
                  />
                </div>

                <div className="form-group">
                  <label style={{ display: "flex" }} htmlFor="terms">
                    <div>
                      <Field
                        id="terms"
                        name="terms"
                        type="checkbox"
                        className={classNames("radio-button")}
                      />
                    </div>
                    <div style={{ paddingLeft: "10px", fontSize: "12px" }}>
                      I would like to be contact by TATA AIG Insurance company
                      limited. It will override my registry at the DNCR.
                    </div>
                  </label>
                </div>

                <button
                  style={{ margin: "30px auto 0" }}
                  type="submit"
                  className="btn btn-secondary btn-block"
                  disabled={isSubmitting}
                >
                  Get a Quote
                </button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
};

export default Car;
