import React, { useState } from "react";
import CustomerAddress from "../../../components/Motor-4W/Proposal/CustomerAddress";
import NomineeDetails from "../../../components/Motor-4W/Proposal/NomineeDetails";
import VehicleDetails from "../../../components/Motor-4W/Proposal/VehicleDetails";
import Accordion from "../../../components/Common/Accordion";

const ProposalDetailsForm = () => {
  const [validationAction, setValidationAction] = useState();
  const [
    validationActionForVehicle,
    setValidationActionForVehicle
  ] = useState();
  const [
    validationActionForNominee,
    setValidationActionForNominee
  ] = useState();

  const setValidationActionWrapper = action => {
    if (!validationAction) {
      setValidationAction(() => () => action());
    }
  };

  const setValidationActionWrapperForVehicle = action => {
    console.log(action);
    if (!validationActionForVehicle) {
      setValidationActionForVehicle(() => () => action());
    }
  };

  const setValidationActionWrapperForNominee = action => {
    if (!validationActionForNominee) {
      setValidationActionForNominee(() => () => action());
    }
  };

  const runValidationAction = () => {
    validationAction();
    console.log('submit', validationAction, validationActionForNominee, validationActionForVehicle)
    if (!validationActionForNominee) {
      
    } else {
      validationActionForNominee();
    }
    if (!validationActionForVehicle) {

    } else{
      validationActionForVehicle();
    }
  };

  return (
    <>
      <div className="row">
        <div
          className="col-12"
          style={{
            border: "1px solid black",
            fontSize: "18px",
            fontWeight: "600",
            margin: "20px 0",
            padding: "20px 15px"
          }}
        >
          View purchase Details
        </div>
        <div className="col-12">
          <div style={{ fontSize: "16px", fontWeight: "600" }}>
            Your Policy is getting generated
          </div>
          <p style={{ fontSize: "14px", fontStyle: "italic" }}>
            Meanwhile please fill the below
          </p>
        </div>
        <div className="col-12">
          <CustomerAddress setValidationAction={setValidationActionWrapper} />
        </div>
        <div className="w-100">
          <Accordion allowMultipleOpen={true}>
            <div label="Vehicle Details">
              <VehicleDetails
                setValidationAction={setValidationActionWrapperForVehicle}
              />
            </div>
            <div label="Nominee Details">
              <NomineeDetails
                setValidationAction={setValidationActionWrapperForNominee}
              />
            </div>
          </Accordion>
        </div>
        <div className="col-12 pt-4">
          <button
            className="btn btn-secondary btn-block"
            onClick={runValidationAction}
          >
            Get My Policy
          </button>
        </div>
      </div>
    </>
  );
};

export default ProposalDetailsForm;
