import App from 'next/app'
import React from 'react'
import Layout from '../components/Layout'
import '../assets/styles/index.scss'
import { Provider } from 'react-redux'
import store from '../redux/store'

export default class MyApp extends App {
	static async getInitialProps({ Component, ctx }) {
		let pageProps = {}

		if (Component.getInitialProps) {
			pageProps = await Component.getInitialProps(ctx)
		}

		return { pageProps }
	}

	render() {
		const { Component, pageProps } = this.props

		return (
			<Provider store={store}>
				<Layout>
					<Component {...pageProps} />
				</Layout>
			</Provider>
		)
	}
}