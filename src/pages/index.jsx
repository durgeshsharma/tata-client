import * as actions from "../redux/actions";
import * as types from "../redux/types";
import { useDispatch, useSelector } from "react-redux";
import Icons from "../components/Landing/Icons";
import Car from "../components/Landing/Car";
import SubHeader from "../components/Landing/SubHeader";

const Index = props => {
  const dispatch = useDispatch();
  const category = useSelector(state => state.businessCategory);
  dispatch(
    actions.changeTitle(
      "General Insurance - Buy or Renew Motor, Travel &amp; Health Insurance Online - TATA AIG"
    )
  );

  return (
    <>
      <Icons />
      <SubHeader />
      {category == types.MOTOR_4W && <Car />}
    </>
  );
};

export default Index;
