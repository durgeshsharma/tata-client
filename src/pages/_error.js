
const Error = ({ statusCode }) => {
	return (
		<div className="container my-5 p-5 border">
			<h1 style={{
				fontSize: 100
			}} className="text-center mt-5">{statusCode}</h1>
			<h1 className="text-center">Whoops !! Looks like we are lost. Try searching again.</h1>
			<div className="d-flex justify-content-center">
				<a href='/' style={{
					padding:'10px 20px',
					borderRadius: 24,
					backgroundColor: '#002042',
					color: "white"
				}} className="btn d-lg-table m-5 text-uppercase">Back to home page</a>
			</div>
		</div>
	)
}

Error.getInitialProps = ({ res, err }) => {
	const statusCode = res ? res.statusCode : err ? err.statusCode : 404
	return { statusCode }
}

export default Error