import * as types from "../types";

const initialState = types.MOTOR_4W;

export default (state = initialState, { type }) => {
  switch (type) {
    case types.MOTOR_4W:
      return type;

    case types.MOTOR_2W:
      return type;

    case types.HEALTH:
      return type;

    case types.TRAVEL:
      return type;

    default:
      return state;
  }
};
