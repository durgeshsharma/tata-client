import { combineReducers } from "redux";
import titleReducer from './title.reducer'
import loaderReducer from './loader.reducer';
import businessCategoryReducer from './business.reducer'

export default combineReducers({
	title: titleReducer,
	loader: loaderReducer,
	businessCategory: businessCategoryReducer
})