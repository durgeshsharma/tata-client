import * as types from '../types'
const initialState =  "General Insurance - Buy or Renew Motor, Travel &amp; Health Insurance Online - TATA AIG"

export default (state = initialState, { type, title }) => {
	switch (type) {
		case types.CHANGE_TITLE:
			return title

		default:
			return state
	}
}
