import * as types from '../types'
export const changeTitle = (title) => {
	return {
		type: types.CHANGE_TITLE,
		title
	}
}