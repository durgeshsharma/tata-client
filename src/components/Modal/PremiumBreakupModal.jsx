import React from "react";
import classNames from 'classnames';

const NoClaimModal = () => {
  return (
    <>
      <div className="row" style={{ fontWeight: "bold" }}>
        <div className="col-9">Own Damage (OD)</div>
        <div className="col-3">3,457/-</div>
      </div>
      <div className="row">
        <div className="col-9">Basic OD</div>
        <div className="col-3">3,457/-</div>
      </div>
      <hr style={{ borderBottom: "1px solid #b4b4b4" }}></hr>
      <div className="row" style={{ fontWeight: "bold" }}>
        <div className="col-9">Liability</div>
        <div className="col-3">2,042/-</div>
      </div>
      <div className="row">
        <div className="col-9">Basic TP</div>
        <div className="col-3">2,042/-</div>
      </div>
      <hr style={{ borderBottom: "1px solid #b4b4b4" }}></hr>
      <div className="row" style={{ fontWeight: "bold" }}>
        <div className="col-9">Total Premium</div>
        <div className="col-3">5,529/-</div>
      </div>
      <div className="row">
        <div className="col-9">Premium</div>
        <div className="col-3">2,042/-</div>
      </div>
      <div className="row">
        <div className="col-9">GST</div>
        <div className="col-3">995/-</div>
      </div>
      <hr style={{ borderBottom: "1px solid #b4b4b4" }}></hr>
      <div className="row" style={{ fontWeight: "bold" }}>
        <div className="col-9">Total Premium</div>
        <div className={classNames("col-3", "strikethrough")}>
          5,529/-
        </div>
      </div>
      <hr style={{ borderBottom: "1px solid #b4b4b4" }}></hr>
      <div className="row" style={{ fontWeight: "bold" }}>
        <div className="col-9">DiscountedPremium</div>
        <div className="col-3">2,042/-</div>
      </div>
      <div className="row pt-5">
        <div className="col-12">
          <button className="btn btn-secondary btn-block">Download Premium Breakup</button>
        </div>
      </div>
    </>
  );
};

export default NoClaimModal;
