import React from "react";

const NoClaimModal = (bonusList) => {
  return (
    <ul className="p-0" style={{ listStyle: "none", fontSize: "20px" }}>
      {bonusList.map((elem, i) => {
        return (
          <li
            key={i}
            style={{
              borderBottom: "1px solid black",
              padding: "10px",
              textAlign: "center"
            }}
            onClick={() => {
              changeClaimBonus(elem);
            }}
          >
            {elem}%
          </li>
        );
      })}
    </ul>
  );
};

export default NoClaimModal;
