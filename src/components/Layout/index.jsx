import Head from "next/head";
import Loader from "../Loader";
import Header from "../Header";
import Footer from "../Footer";
import React from "react";
import classes from "./Layout.module.scss";
import { useSelector } from "react-redux";

export default ({ children }) => {
  const title = useSelector(state => state.title);
  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <Loader />
      <header>
        <Header />
      </header>
      <main className={classes.main}>
        <div className="container">{children}</div>
      </main>
      <footer>{/* <Footer /> */}</footer>
    </>
  );
};
