import React from "react";
import classNames from "classnames";
import classes from "./SubHeader.module.scss";

const SubHeader = () => {
  return (
    <div className="row d-flex" style={{padding: '10px 0 40px'}}>
      <div className="col-12">
      <div style={{ fontWeight: "600", fontSize: "20px" }}>Welcome!</div>
      <div style={{ fontStyle: "italic", fontSize: "14px" }}>
        Kindly enter your details to continue
      </div>
      </div>
    </div>
  );
};

export default SubHeader;
