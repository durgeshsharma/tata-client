import React from "react";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../../../redux/actions";
import classNames from "classnames";
import classes from "./Icons.module.scss";
import { categoryList } from "../CategoryList";

const DashboardNav = () => {
  const category = useSelector(state => state.businessCategory);
  const dispatch = useDispatch();

  let changeCategory = type => {
    dispatch(actions.changeBusinessCategory(type));
  };

  return (
    <ul className={classNames("nav", classes.customNav)}>
      {categoryList.map((item, index) => (
        <li
          key={index}
          className="nav-item"
          onClick={() => changeCategory(item.type)}
        >
          <div className={classNames("text-center", classes.quickIcon)}>
            <img
              src={item.src}
              className={classNames(category === item.type && "round")}
              alt="icon"
            />
            <h6>{item.title}</h6>
          </div>
        </li>
      ))}
    </ul>
  );
};

export default DashboardNav;
