import * as types from '../../redux/types'

export const categoryList = [
	{
		title: 'Car',
		src: './motor.png',
		type: types.MOTOR_4W
	},
	{
		title: 'Medical',
		src: './health.png',
		type: types.HEALTH
	},
	{
		title: 'Travel',
		src: './travel.png',
		type: types.TRAVEL
	},
	{
		title: '2-wheeler',
		src: './bike.png',
		type: types.MOTOR_2W
	}	
]