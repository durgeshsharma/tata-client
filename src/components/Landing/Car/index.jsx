import React from "react";
import Link from "next/link";
import { Formik, Form, Field, ErrorMessage } from "formik";
import classNames from "classnames";
import "./Car.module.scss";

const Car = () => {
  let initialForm = {
    registrationNo: ""
  };

  const registrationTest = /^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$/;

  let Validator = values => {
    let errors = {};
    if (values.registrationNo === "") {
      errors.registrationNo = "Registration Number is required";
    } else if (!registrationTest.test(values.registrationNo)) {
      errors.registrationNo = "Invalid Format";
    }
    return errors;
  };

  return (
    <>
      <div className="row">
        <div className="col-12">
          <Formik
            initialValues={initialForm}
            validate={Validator}
            onSubmit={({ setSubmitting }) => {
              setSubmitting(false);
            }}
          >
            {({ touched, errors, isSubmitting }) => (
              <Form>
                <div className="form-group">
                  <label style={{ fontSize: "14px" }} htmlFor="registration">
                    Registration No
                  </label>
                  <Field
                    id="registration"
                    type="text"
                    name="registrationNo"
                    placeholder="MH 01 AB 1234"
                    className={`form-control ${
                      touched.registrationNo && errors.registrationNo
                        ? "is-invalid"
                        : ""
                    }`}
                  />
                  <ErrorMessage
                    component="div"
                    name="registrationNo"
                    className="invalid-feedback"
                  />
                </div>

                <div
                  className="form-group text-center"
                  style={{ margin: "40px auto", fontSize: "14px" }}
                >
                  <Link href="/motor-four-wheeler">
                    <a style={{ textDecoration: "underline", color: "black" }}>
                      I don't know my registration number
                    </a>
                  </Link>
                </div>

                <button style={{margin: '30px auto 0'}}
                  type="submit"
                  className="btn btn-primary btn-block"
                  disabled={isSubmitting}
                >
                  Get a Quote
                </button>
                <button style={{margin: '10px auto'}} className="btn btn-secondary btn-block">
                  I want to renew
                </button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
};

export default Car;
