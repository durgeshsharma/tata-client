import classNames from 'classnames';
import classes from './Footer.module.scss'
import { quickLinks } from './footerLinks'
import Link from 'next/link'

const QuickLinks = () => {

	return (
		<div className={classNames("container-fluid py-3 my-2", classes.quickLinksContainer)}>
			<div className="row px-5">
				{
					quickLinks.map((quickLink, index) => (
						<div key={index} className={classNames("col-lg-3 col-md-6", classes.quickLinkList)}>
							<h4>{quickLink.heading}</h4>
							<ul className="nav flex-column">
								{
									quickLink.links.map((link, index) => (
										<li key={index} className="nav-item">
											<Link prefetch={false} href={link.link}><a className="nav-link">{link.name}</a></Link>
										</li>
									))
								}
							</ul>
						</div>
					))
				}
			</div>
		</div>
	)
}

export default QuickLinks