import QuickLinks from './QuickLinks'
import BaseFooter from './BaseFooter'

const Footer = props => {
	return (
		<>
			<QuickLinks />
			<BaseFooter />
		</>
	)
}

export default Footer