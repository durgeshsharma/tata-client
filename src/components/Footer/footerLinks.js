export const pdfLinks = [
	{
		name: 'individual agent list',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/AgentLists/List_Of_Individual_Agents_TAGIC.pdf'
	},
	{
		name: 'corporate agent list',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/AgentLists/List_Of_Corporate_Agents_TAGIC.pdf'
	},
	{
		name: 'suspended agent list',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/AgentLists/List_Of_Suspended_Agents_TAGIC.pdf'
	},
	{
		name: 'Hudhud Cyclone Claim Settlement Status',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	}

]

export const footerLinks = [
	{
		name: 'Terms of use',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	},
	{
		name: 'privacy',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	},
	{
		name: 'Sitemap',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	},
	{
		name: 'Brokers',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	},
	{
		name: 'Tata Group',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	},
	{
		name: 'AIG',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	},
	{
		name: 'Chartis',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	},
	{
		name: 'Tata AIA Life Insurance',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	},
	{
		name: 'IRDA CEW',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	},
	{
		name: 'US Visitor Visa',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	},
	{
		name: 'Tata Medical Center',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	},
	{
		name: 'Insurance Institute of India',
		link: 'https://www.tataaig.com/content/dam/tagic/PDF/CycloneClaimSettlement/hudhud-cyclone-claim-settlement-status.pdf'
	}
]


export const quickLinks = [
	{
		heading: "FOR CUSTOMERS",
		links: [
			{
				name: "Policy Wordings",
				link: '/'
			},
			{
				name: "Proposal Forms",
				link: '/'
			},
			{
				name: "Claims Forms",
				link: '/'
			},
			{
				name: "Customer Information Sheet",
				link: '/'
			},
			{
				name: "Ombudsman Related Formats",
				link: '/'
			},
			{
				name: "Knowledge Center",
				link: '/'
			},
			{
				name: "IRDA Notification",
				link: '/'
			},
			{
				name: "Unclaimed Amount",
				link: '/'
			},
			{
				name: "General FAQs",
				link: '/'
			},
			{
				name: "Motor TP Rates 2019-20",
				link: '/'
			},
			{
				name: "Motor Third Party Liability",
				link: '/'
			},
			{
				name: "Protection of Policyholders Interests Regulations",
				link: '/'
			},
			{
				name: "Grievance Redressal Policy",
				link: '/'
			},
			{
				name: "Service Parameters",
				link: '/'
			},
			{
				name: "Health TPA List",
				link: '/'
			},
			{
				name: "Disclaimer",
				link: '/'
			}
		]
	},
	{
		heading: 'SERVICE',
		links: [

			{
				name: "Ask our experts",
				link: '/'
			},
			{
				name: "Find your nearest branch",
				link: '/'
			},
			{
				name: "Find your nearest hospital",
				link: '/'
			},
			{
				name: "Car & Motor Service Centers",
				link: '/'
			},
			{
				name: "Send us your feedback",
				link: '/'
			},
			{
				name: "Service Related FAQs",
				link: '/'
			},
			{
				name: "Pay Now",
				link: '/'
			},
			{
				name: "Pay Now- Save and Retrieve",
				link: '/'
			},
			{
				name: "Update PUC Details",
				link: '/'
			},
			{
				name: "Update your Contact Details",
				link: '/'
			}
		]
	},
	{
		heading: 'OUR PRODUCTS',
		links: [
			{
				name: "Travel Insurance",
				link: '/insurance/travel-insurance'
			},
			{
				name: "Motor Insurance",
				link: '/insurance/motor-insurance'
			},
			{
				name: "Health Insurance",
				link: '/insurance/health-insurance'
			},
			{
				name: "Accident Insurance",
				link: '/insurance/personal-accident'
			},
			{
				name: "Home Insurance",
				link: '/'
			},
			{
				name: "Student Travel Insurance",
				link: '/'
			},
			{
				name: "International Travel Insurance",
				link: '/'
			},
			{
				name: "Pradhan Mantri Fasal Bima Yojana (PMFBY)",
				link: '/'
			},
			{
				name: "Auto Secure-Private Car Package Policy",
				link: '/'
			},
			{
				name: "Rural Insurance",
				link: '/'
			},
			{
				name: "Product Related FAQs",
				link: '/'
			},
			{
				name: "Product’s UIN",
				link: '/'
			},
			{
				name: "List Of Withdrawn Products",
				link: '/'
			}
		]
	},
	{
		heading: 'COMPANY',
		links: [

			{
				name: "About Us",
				link: '/about-us'
			},
			{
				name: "Careers",
				link: '/'
			},
			{
				name: "Current Openings",
				link: '/'
			},
			{
				name: "Campus Hiring",
				link: '/'
			},
			{
				name: "Media Centre",
				link: '/MediaCentre'
			},
			{
				name: "CSR",
				link: '/'
			},
			{
				name: "Board of Directors",
				link: '/'
			},
			{
				name: "Public Disclosures",
				link: '/'
			},
			{
				name: "Key Persons",
				link: '/'
			},
			{
				name: "Young Achievers",
				link: '/'
			},
			{
				name: "Stewardship Policy",
				link: '/'
			},
			{
				name: "Corporate Disclosures for Debt Securities",
				link: '/'
			},
			{
				name: "GST Registration Numbers",
				link: '/'
			},
			{
				name: "Plan Your Travel",
				link: '/'
			}
		]
	}
]

