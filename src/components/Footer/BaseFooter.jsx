import classNames from 'classnames';
import classes from './Footer.module.scss'
import { pdfLinks, footerLinks } from './footerLinks'

const BaseFooter = () => {
	return (
		<div className="container-fluid">
			<div className={classNames("row d-flex justify-content-center")}>
				<ul className={classNames("nav", classes.footerNavList)}>
					{
						pdfLinks.map((link, index) => (
							<li className="nav-item" key={index}>
								<a className="nav-link text-capitalize" href={link.link}>{link.name}</a>
							</li>
						))
					}
				</ul>
			</div>
			<div className={classNames("row my-3 d-flex justify-content-center")}>
				<ul className={classNames("nav", classes.footerNavList)}>
					{
						footerLinks.map((link, index) => (
							<li className="nav-item" key={index}>
								<a className="nav-link" href={link.link}>{link.name}</a>
							</li>
						))
					}
				</ul>
			</div>
			<div className={classNames("row d-flex justify-content-center mb-3", classes.footerDescription)}>
				<p>2008, Tata AIG General Insurance Company Limited, all rights reserved.</p>
				<p>Registered Office : Peninsula Business Park, Tower A, 15thFloor, G.K.Marg, Lower Parel, Mumbai-400 013, Maharashtra, India. CINNumber : U85110MH2000PLC128425.</p>
				<p>Registered with <a href="https://www.tataaig.com/#">IRDA of India Regn. No. 108.</a> Insurance is the subject matter of the solicitation.</p>
				<p>For more details on benefits, exclusions, limitations, terms and conditions, please read sales brochure/policy wording carefully before concluding a sale.</p>
				<p>Toll Free Number : 1800 266 7780 / 1800 22 9966 (only for senior citizen policy holders). Email Id –customersupport@tataaig.com. Fax Number – 022 66938170</p>
			</div>
		</div>
	)
}

export default BaseFooter