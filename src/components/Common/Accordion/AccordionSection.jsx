import React, { Component } from "react";
import PropTypes from "prop-types";

class AccordionSection extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Object).isRequired,
    isOpen: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  };


  onClick = () => {
    this.props.onClick(this.props.label);
  };

  render() {
    const {
      onClick,
      props: { isOpen, label }
    } = this;

    return (
      <div
        style={{
          border: !isOpen && "1px solid black",
          padding: "10px 10px"
        }}
      >
        <div onClick={onClick} style={{ cursor: "pointer" }}>
          {label}
          <div style={{ float: "right" }}>
            {!isOpen && <span><i className="fa fa-plus" aria-hidden="true"></i></span>}
            {isOpen && <span><i className="fa fa-minus" aria-hidden="true"></i></span>}
          </div>
        </div>
        
          <div
            style={{
              marginTop: 20, display: (isOpen ? "block": "none")
            }}
          >
            {this.props.children}
          </div>
        
      </div>
    );
  }
}

export default AccordionSection;
