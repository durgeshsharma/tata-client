import React, { Component } from "react";
import classNames from "classnames";
import classes from './ToggleSwitch.module.scss';

class ToggleSwitch extends Component {
  render() {
    return (
      <div className={classNames(classes.toggle_switch)}>
        <input
          type="checkbox"
          className={classNames(classes.toggle_switch_checkbox)}
          name={this.props.Name}
          id={this.props.Name}
          onChange={()=> {this.props.handleToggle()}}
        />
        <label className={classNames(classes.toggle_switch_label)} htmlFor={this.props.Name}>
          <span className={classNames(classes.toggle_switch_inner)} />
          <span className={classNames(classes.toggle_switch_switch)} />
        </label>
      </div>
    );
  }
}

export default ToggleSwitch;