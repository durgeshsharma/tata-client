import React, { useEffect } from "react";
import { withFormik, Form, Field, ErrorMessage } from "formik";
import classes from "../Motor-4W.module.scss";

const CustomerAddress = props => {
  const { touched, errors, handleSubmit } = props;

  return (
    <Form onSubmit={handleSubmit}>
      <div className="form-group">
        <Field id="address"
          type="text"
          name="address"
          placeholder="HOUSE / FLAT / BLOCK NUMBER"
          className={`form-control ${
            touched.address && errors.address ? "is-invalid" : ""
          }`}
        />
        <ErrorMessage
          component="div"
          name="address"
          className="invalid-feedback"
        />
      </div>

      <div className="form-group">
        <Field id="landmark"
          type="text"
          name="landmark"
          placeholder="LANDMARK"
          className={`form-control ${
            touched.landmark && errors.landmark ? "is-invalid" : ""
          }`}
        />
        <ErrorMessage
          component="div"
          name="landmark"
          className="invalid-feedback"
        />
      </div>

      <div className="form-group">
        <Field id="pincode"
          type="text"
          name="pincode"
          placeholder="PIN CODE"
          className={`form-control ${
            touched.pincode && errors.pincode ? "is-invalid" : ""
          }`}
        />
        <ErrorMessage
          component="div"
          name="pincode"
          className="invalid-feedback"
        />
      </div>
    </Form>
  );
};

const CustomerAddressWrapper = ({
  setValidationAction,
  submitForm,
  ...other
}) => {
  useEffect(() => {
    setValidationAction(submitForm);
  }, [submitForm]);
  return <CustomerAddress {...other} />;
};

const CustomerAddressEnhanced = withFormik({
  mapPropsToValues: () => ({
    address: "",
    landmark: "",
    pincode: ""
  }),

  validate: values => {
    const errors = {};

    if (values.address === "") {
      errors.address = "House/Flat/Block Number is required";
    } 
    if (values.landmark === "") {
      errors.landmark = "Landmark is required";
    } 
    if (values.pincode === "") {
      errors.pincode = "Pincode is required";
    }
    return errors;
  },

  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      setSubmitting(false);
    }, 1000);
  }
})(CustomerAddressWrapper);

export default CustomerAddressEnhanced;
