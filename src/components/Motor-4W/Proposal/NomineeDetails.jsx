import React, { useEffect } from "react";
import { withFormik, Form, Field, ErrorMessage } from "formik";
import classes from "../Motor-4W.module.scss";

const NomineeDetails = props => {
  const { values, touched, errors, handleSubmit } = props;

  return (
    <div className="row">
      <div className="col-12">
        <Form onSubmit={handleSubmit}>
          <div className="form-group">
            <Field id="name"
              type="text"
              name="name"
              placeholder="Enter name"
              className={`form-control ${
                touched.name && errors.name ? "is-invalid" : ""
              }`}
            />
            <ErrorMessage
              component="div"
              name="name"
              className="invalid-feedback"
            />
          </div>

          <div className="form-group">
            <Field id="dob"
              type="text"
              name="dob"
              placeholder="DD/MM/YY"
              className={`form-control ${
                touched.dob && errors.dob ? "is-invalid" : ""
              }`}
            />
            <ErrorMessage
              component="div"
              name="dob"
              className="invalid-feedback"
            />
          </div>

          <div className="form-group">
            <Field id="relation"
              type="select"
              name="relation"
              placeholder="Select relation"
              className={`form-control ${
                touched.relation && errors.relation ? "is-invalid" : ""
              }`}
            />
            <ErrorMessage
              component="div"
              name="relation"
              className="invalid-feedback"
            />
          </div>
        </Form>
      </div>
    </div>
  );
};

const NomineeDetailsWrapper = ({
  setValidationAction,
  submitForm,
  ...other
}) => {
  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      setValidationAction(submitForm);
    }
    return () => {
      isMounted = false;
    };
  });
  return <NomineeDetails {...other} />;
};

const NomineeDetailsEnhanced = withFormik({
  mapPropsToValues: () => ({
    name: "",
    dob: "",
    relation: ""
  }),

  validate: values => {
    let errors = {};
    if (values.name === "") {
      errors.name = "Nominee is required";
    } 
    if (values.dob === "") {
      errors.dob = "Date of birth is required";
    } 
    if (values.relation === "") {
      errors.relation = "Relation is required";
    }
    return errors;
  },

  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      setSubmitting(false);
    }, 1000);
  }
})(NomineeDetailsWrapper);

export default NomineeDetailsEnhanced;
