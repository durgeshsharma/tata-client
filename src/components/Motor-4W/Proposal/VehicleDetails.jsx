import React, { useState, useEffect } from "react";
import { withFormik, Form, Field, ErrorMessage } from "formik";
import classes from "../Motor-4W.module.scss";
import Modal from "../../Modal";
import ChassisNumberModal from "../../Modal/ChassisNumberModal";
import EngineNumberModal from "../../Modal/EngineNumberModal";
import LoanModal from "../../Modal/LoanModal";
import ToggleSwitch from "../../Common/ToggleSwitch";

const VehicleDetails = props => {
  const { values, touched, errors, handleSubmit } = props;
  const [showEngineModal, setShowEngineModal] = useState(false);

  const engineModalClose = () => setShowEngineModal(false);
  const engineModalShow = () => setShowEngineModal(true);

  const [showChassisModal, setShowChassisModal] = useState(false);

  const chassisModalClose = () => setShowChassisModal(false);
  const chassisModalShow = () => setShowChassisModal(true);

  const [showLoanModal, setShowLoanModal] = useState(false);

  const loanModalClose = () => setShowLoanModal(false);
  const loanModalShow = () => setShowLoanModal(true);

  const [toggle, setToggle] = useState(false);

  const handleToggle = () => {
    setToggle(!toggle);
  };

  return (
    <div className="row">
      <div className="col-12">
            <Form onSubmit={handleSubmit}>
              <div className="form-group">
                <label style={{ fontSize: "14px" }}>
                  ENGINE NUMBER
                  <i
                    className="fa fa-question-circle"
                    onClick={() => {
                      engineModalShow();
                    }}
                  ></i>
                </label>
                <Field
                  id="vehicleNo"
                  type="text"
                  name="vehicleNo"
                  placeholder="Enter Engine Number"
                  className={`form-control ${
                    touched.vehicleNo && errors.vehicleNo ? "is-invalid" : ""
                  }`}
                />
                <ErrorMessage
                  component="div"
                  name="vehicleNo"
                  className="invalid-feedback"
                />
              </div>
              <Modal
                toggle={() => {
                  engineModalClose();
                }}
                modal={showEngineModal}
                children={EngineNumberModal()}
                title="Engine Number"
              />
              <div className="form-group">
                <label style={{ fontSize: "14px" }} >
                  CHASSIS NUMBER
                  <i
                    className="fa fa-question-circle"
                    onClick={() => {
                      chassisModalShow();
                    }}
                  ></i>
                </label>
                <Field
                  id="chassisNo"
                  type="text"
                  name="chassisNo"
                  placeholder="Chassis Number"
                  className={`form-control ${
                    touched.chassisNo && errors.chassisNo ? "is-invalid" : ""
                  }`}
                />
                <ErrorMessage
                  component="div"
                  name="chassisNo"
                  className="invalid-feedback"
                />
              </div>
              <Modal
                toggle={() => {
                  chassisModalClose();
                }}
                modal={showChassisModal}
                children={ChassisNumberModal()}
                title="Chassis Number"
              />
              <div className="form-group">
                <label style={{ fontSize: "14px" }} htmlFor="financiarName">
                  IS YOUR HONDA CITY ON LOAN
                  <i
                    className="fa fa-question-circle"
                    onClick={() => {
                      loanModalShow();
                    }}
                  ></i>
                </label>
                <div><ToggleSwitch Name="toggle" handleToggle={handleToggle} /></div>
                {toggle && (
                  <>
                    <Field
                      id="financiarName"
                      type="text"
                      name="financiarName"
                      placeholder="FINANCIAR NAME"
                      className={`form-control ${
                        touched.financiarName && errors.financiarName
                          ? "is-invalid"
                          : ""
                      }`}
                    />
                    <ErrorMessage
                      component="div"
                      name="financiarName"
                      className="invalid-feedback"
                    />
                  </>
                )}
              </div>
              <Modal
                toggle={() => {
                  loanModalClose();
                }}
                modal={showLoanModal}
                children={LoanModal()}
                title="Loan"
              />
              <div className="form-group">
                <label style={{ fontSize: "14px" }} htmlFor="prevPolicy">
                  PREVIOUS POLICY NUMBER
                </label>
                <Field
                  id="prevPolicy"
                  type="text"
                  name="prevPolicy"
                  placeholder="Previous Policy"
                  className={`form-control ${
                    touched.prevPolicy && errors.prevPolicy ? "is-invalid" : ""
                  }`}
                />
                <ErrorMessage
                  component="div"
                  name="prevPolicy"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label style={{ fontSize: "14px" }} htmlFor="prevInsurer">
                  PREVIOUS INSURER NAME
                </label>
                <Field
                  id="prevInsurer"
                  type="select"
                  name="prevInsurer"
                  placeholder="Select Insurer"
                  className={`form-control ${
                    touched.prevInsurer && errors.prevInsurer ? "is-invalid" : ""
                  }`}
                />
                <ErrorMessage
                  component="div"
                  name="prevInsurer"
                  className="invalid-feedback"
                />
              </div>
            </Form>
      </div>
    </div>
  );
};

const VehicleDetailsWrapper = ({
  setValidationAction,
  submitForm,
  values,

  ...other
}) => {
  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      setValidationAction(submitForm);
    }
    return () => {
      isMounted = false;
    };
  });
  return <VehicleDetails {...other} />;
};

const VehicleDetailsEnhanced = withFormik({
  mapPropsToValues: () => ({
    vehicleNo: "",
    chassisNo: "",
    financiarName: "",
    prevPolicy: "",
    prevInsurer: ""
  }),

  validate: values => {
    let errors = {};
    if (values.vehicleNo === "") {
      errors.vehicleNo = "Vehicle Number is required";
    } 
    if (values.chassisNo === "") {
      errors.chassisNo = "Chasis Number is required";
    } 
    if (values.prevPolicy === "") {
      errors.prevPolicy = "Previous Policy is required";
    } 
    if (values.prevInsurer === "") {
      errors.prevInsurer = "Previous Insurer is required";
    } 
    return errors;
  },

  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      setSubmitting(false);
    }, 1000);
  }
})(VehicleDetailsWrapper);

export default VehicleDetailsEnhanced;
