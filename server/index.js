const express = require('./middleware/express')
const app = require('./middleware/next')
const { port, appRoot, ip, env, mongo } = require('./config')
const routes = require('./router');
const handle = app.getRequestHandler();
var { MongoClient } = require('mongodb');
const ssrCache = require('./middleware/LRUCache');

app.prepare().then(() => {
	const server = express(appRoot, routes);
	// MongoClient.connect(mongo.uri,(error, db)=>{
	// 	if(error) console.log(error)
	// 	const changeStream = db.watch();
	// 	changeStream.on('change', (data)=>{
	// 		console.log(data);
	// 		ssrCache.reset();
			
	// 	})
	// });

	server.listen(port, err => {
		if (err) throw err
		console.log(`> Ready on http://${ip}:${port} in ${env} mode`)
	})
})
