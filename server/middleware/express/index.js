const express = require('express');
const path = require('path');

module.exports = (apiRoot, routes) => {
	const app = express();
	app.use(express.static(path.join(__dirname, '../../../public')));

	app.use(apiRoot, routes);

	return app;
}