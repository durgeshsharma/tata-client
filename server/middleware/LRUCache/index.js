const LRUCache = require('lru-cache');
const { cache } = require('../../config');

// This is where we cache our rendered HTML pages
const ssrCache = new LRUCache({
	max: cache.maxSize || 100 * 1024 * 1024,
	length: function (n, key) {
		return n.length
	},
	maxAge: cache.maxAge || 1000 * 60 * 60 * 24 * 30
});
module.exports = ssrCache;