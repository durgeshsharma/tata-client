const router = require('express').Router(); 
const app = require('../middleware/next');
const handle = app.getRequestHandler();
const authRoute = require('./auth');
const appRoute = require('./app');
const ssrCache = require('../middleware/LRUCache');

function getCacheKey(req) {
    return `${req.path}`
}

async function renderAndCache(req, res) {	
    const key = getCacheKey(req);

	// If we have a page in the cache, let's serve it
    if (ssrCache.has(key)) {
        // console.log(`serving from cache ${key}`);
        res.setHeader('x-cache', 'HIT');
        res.send(ssrCache.get(key));
        return
	}
	
    try {
        //console.log(`key ${key} not found, rendering`);
        // If not let's render the page into HTML
		const html = await app.renderToHTML(req, res, req.path, req.query);		

        // Something is wrong with the request, let's skip the cache
        if (res.statusCode !== 200) {
            res.send(html);
            return
        }

        // Let's cache this page
        // ssrCache.set(key, html);

        res.setHeader('x-cache', 'MISS');
		res.send(html)
    } catch (err) {
        app.renderError(err, req, res, req.path, req.query)
    }
}

router.use('/auth', authRoute)

router.use('/app', appRoute)

router.get('/_next/*', (req, res) => {
	/* serving _next static content using next.js handler */
	return handle(req, res);
});

router.all('*', (req, res) => {
	return renderAndCache(req, res)
})

module.exports = router;